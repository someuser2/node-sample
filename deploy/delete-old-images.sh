#!/bin/bash

IMAGE_NAME=${1-$IMAGE}
docker image ls | awk '/'$IMAGE_NAME'/{print $1":"$2}'  | while read i; do docker image rm $i; done
