var app = require('./app');

const PORT = process.env.PORT || 10080;
const HOST = process.env.ADDR || '0.0.0.0';

app.listen(PORT,HOST);
