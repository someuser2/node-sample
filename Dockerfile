FROM node:10-slim
WORKDIR /home/node-sample
COPY package*.json app.js node-sample.js test ./
RUN npm install
EXPOSE 10080
CMD [ "npm", "start" ]
