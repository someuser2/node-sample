var express = require('express');
var app = express();

app.get('/sayhello/?:text', function(req, res){
    res.writeHead(200, { 'Content-Type': 'text/plain' });
    res.write(`Hello, ${req.params.text}!`);
    res.end();
});

app.get('/sayhello', function(req, res){
    res.writeHead(200, { 'Content-Type': 'text/plain' });
    res.write(req.query.name?`Hello, ${req.query.name}!`:'Hello!');
    res.end();
});

app.get('/', function(req, res){
    res.writeHead(200, { 'Content-Type': 'text/plain' });
    res.write("OK");
    res.end();
});

module.exports = app;

