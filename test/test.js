var request = require('supertest')
    , express = require('express');

var app = require('../app');

describe('GET /', function(){
    it('GET / text is "OK"', function(done){
      request(app)
      .get('/')
      .set('Accept', 'text/plain')
      .expect('Content-Type', 'text/plain')
      .expect(200,"OK",done)
    })
    it('GET /test is  "404 Not Found"', function(done){
      request(app)
      .get('/test')
      .expect(404,done)
    })
    it('GET /sayhello?name=test', function(done){
      request(app)
      .get('/sayhello?name=test')
      .set('Accept', 'text/plain')
      .expect('Content-Type', 'text/plain')
      .expect(200,"Hello, test!",done)
    })
    it('GET /sayhello', function(done){
      request(app)
      .get('/sayhello')
      .set('Accept', 'text/plain')
      .expect('Content-Type', 'text/plain')
      .expect(200,"Hello!",done)
    })
    it('GET /sayhello/', function(done){
      request(app)
      .get('/sayhello')
      .set('Accept', 'text/plain')
      .expect('Content-Type', 'text/plain')
      .expect(200,"Hello!",done)
    })
    it('GET /sayhello/test', function(done){
      request(app)
      .get('/sayhello/test')
      .set('Accept', 'text/plain')
      .expect('Content-Type', 'text/plain')
      .expect(200,"Hello, test!",done)
    })
    it('GET /sayhello/test/', function(done){
      request(app)
      .get('/sayhello/test')
      .set('Accept', 'text/plain')
      .expect('Content-Type', 'text/plain')
      .expect(200,"Hello, test!",done)
    })
    
})
